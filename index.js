console.log("Hello World");

/*
	1. Create variables to store to the following user details:

	-first name - String
	-last name - String
	-age - Number
	-hobbies - Array
	-work address - Object

		-The hobbies array should contain at least 3 hobbies as Strings.
		-The work address object should contain the following key-value pairs:

			houseNumber: <value>
			street: <value>
			city: <value>
			state: <value>

	Log the values of each variable to follow/mimic the output.

	Note:
		-Name your own variables but follow the conventions and best practice in naming variables.
		-You may add your own values but keep the variable names and values Safe For Work.
*/

	//Add your variables and console log for objective 1 here:




/*			
	2. Debugging Practice - Identify and implement the best practices of creating and using variables 
	   by avoiding errors and debugging the following codes:

			-Log the values of each variable to follow/mimic the output.
*/	
/*DEBUGGING ACTIVITYYYYYYY*/

	let fullName = "Steve Rogers";
	let name = fullName;
	console.log("My full name is" +" "+ name);

	let age = 40;
	let currentAge = age;
	console.log("My current age is: " + currentAge);
	
	let friends = ["Tony","Bruce","Thor","Natasha","Clint","Nick"];
	console.log("My Friends are: \n\n" + friends);
	console.log(friends);

	let profile = {

		username: "captain_america",
		fullName: "Steve Rogers",
		age: 40,
		isActive: false,

	};


	/*CREATING VARIABLES ACTIVITYYYYYYY*/ 

	console.log("My Full Profile: ");
	console.log(profile);

	let fullName1 = "Bucky Barnes";
	console.log("My bestfriend is: " + fullName1);

	let lastLocation = "Arctic Ocean";
	lastLocation = "Atlantic Ocean";
	console.log("I was found frozen in: " + lastLocation);

	let firstName = "Jam";
	console.log("First Name: " + firstName);

	let lastName = "Marcial";
	console.log("Last Name: " + lastName);

	let age1 = 21;
	console.log("Age: " + age1);

	let hobbies = ['Gaming', 'Eating', 'Sleeping'];
	console.log("Hobbies: " + hobbies);

	let workAddress = {
		houseNumber: "27",
		street: "Quezon Street",
		city: "Rizal",
		isActive: true
		};
			console.log(workAddress);

